import { createStore } from 'vuex';
import verbs from './verbs.js';

// Initialize Vuex
const store = createStore({
    modules: {
        verbs: verbs
    }
});

export function useStore() {
    return store;
}

export default store;