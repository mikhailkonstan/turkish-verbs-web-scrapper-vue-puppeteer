import axios from 'axios';

const state = () => {
    return {
        verbs: [],
        selectedVerbs: [],
    }
};

const getters = {
    verbs(state) {
        return state.verbs;
    },
    selectedVerbs(state) {
        return state.selectedVerbs;
    }
};

const mutations = {
    SET_VERBS (state, payload){
        state.verbs = payload;
    },
    SET_SELECTED_VERBS(state, payload){
        state.selectedVerbs = payload;
    }
};

const actions = {
    /**
     * Makes an API request to the backend in order to scrap data from the web. 
     * Afterwards, the scrapped verbs are loaded into this state
     * @param {*} context 
     */
    async scrapData(context){
        let verbs = {};

        await axios.get('http://localhost:3001/api/verbs')
            .then(res => {
                verbs = res.data.data;
            });
        
        context.commit('SET_VERBS', verbs);
    },

    /**
     * From the verbs imported in this state, choose a number of random X verbs
     * X is defined in payload
     * 
     * Payload should be in the format { words : number }
     * @param {*} context 
     * @param {*} payload 
     */
    generateRandomWords(context, payload){
        let selectedVerbs = {};

        var keys = Object.keys(context.state.verbs);
        let randomKey;

        // todo: perhaps add a words exist and words > 0 check
        for(let i=0; i<payload.words; i++){

            // Find a unique random key
            do {
                randomKey = keys.length * Math.random() << 0;
            } while((keys[randomKey] in selectedVerbs))

            // Add verb to the list
            selectedVerbs[keys[randomKey]] = context.state.verbs[keys[ randomKey ]];
        }

        context.commit('SET_SELECTED_VERBS', selectedVerbs);
    }

}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}