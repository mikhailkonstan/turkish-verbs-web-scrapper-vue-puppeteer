# Dictionary scrapper 
##### :notebook: A web scrapper project for a Turkish-to-Greek dictionary

## :computer: Technologies used
- Bulma (CSS Framework)
- Vue.js
  - Vuex
  - Axios
- Puppeteer
- Cheerio
- Node.js
  - Express.js

## :dart: Project goals
### Personal
My personal goal was to learn more about a topic that I was curious to find out how it works: **Web scrap**. Furthermore, I wanted to improve my skills in JavaScript and Vue.js (3rd edition) combined with official packages and APIs such as the **Composition API** and **VUEX**.
Additionally, I wanted to create an application that would help me with my university courses as well. An idea came up when I wanted to expand my vocabulary on the turkish language :tr: which I took in my university. 
### Application
The website aims to fetch data from the internet and and provide a random list of **10 verbs** with their equivalent translation in **Greek** :gr:
## :book:  How to install
- Make sure you have the lastest version of Node.js
- Execute the following commands to install all dependencies
```bash
npm install
```
- Execute the following commands to test the application

```bash
# Development mode
npm run serve

# Or if you want to upload the project to your server
npm run build
```
```bash
# Start back-end server ReST API
node app.js
```

### How to use
First of all, we need to scrap data from the internet and load them in this page. Then we need will be able to select random verbs
![homepage](docImages/webscrap.png)
1. In order to fetch data, press the button <strong>scrap data</strong>
2. Once data are scrapped, you will see that the button <strong>Select 10 random verbs</strong> is now available. Press the button to get 10 random words from the system.

![endresult](docImages/webscrap2.png)

## :open_file_folder: Program strucutre
The program is divided in two main parts
1. Front-end
2. Back-end API

### Back-end API
Since this is a small program, you will find all nescessary code in the file **app.js**. It contains only a couple of functions for routing and a function responsible for scrapping data from the website of [pgeorgalas](http://turkish.pgeorgalas.gr/).

### Front-end 
The front-end has the basic structure of any Vue.js application. The application is located in **src** directory

##### Set the correct API url
The application's source code is predefined to search for localhost on port **3001** but it is obvious that when deploying this application, a different host and a different port will occur (most probably). The API call is done using VUEX state manager, meaning that you need to locate the store manager. 
- src
  - index.js
  - **verbs.js** -> Contains the state manager for the verbs

To change the url simply locate the **action** methods located in the file **verbs.js** and change the url used for axios


