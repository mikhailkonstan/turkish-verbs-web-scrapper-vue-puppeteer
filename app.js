const express = require('express');
const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const app = express();
var path = require('path');
var cors = require('cors');

app.use(cors());

/**
 * Visits Georgalas' website and returns all verbs with a translation in Greek
 * 
 * The function is responsible to enter the website, integrate with the website and load
 * the maximum available threads. Afterwards, the website uses Cheerio to load every single
 * row of turkish verb-translation pairs. Creates an object in the form of key=>translation where
 * the 'key' is equal to the verb in Turkish.
 * 
 * @return Verbs object in Turkish-Greek
 */
const scrapVerbs = async () => {
    // Return values
    const dictionary = {};

    const browser = await puppeteer.launch( {headless: true });
    const page = await browser.newPage();
    await page.goto('http://turkish.pgeorgalas.gr/VerbsTheorySet.asp', {
        waitUntil: 'domcontentloaded'
    });
    
    // Click on "Verbs for advanced" to load the table that contains all available verbs
    await page.click('input.button');
    await page.waitForTimeout(2000);
    await page.screenshot({path: '1.png', fullPage: true});

    // Initialize Cheerio with the current loaded HTML view
    const html = await page.evaluate( () => document.body.innerHTML);
    const $ = cheerio.load(html);

    // Get all available verbs (Note: First three rows are for header thus are skipped)
    const verbsContainer = $('.TableTr > tbody > tr:gt(2)');
    for(let i = 0;i<(verbsContainer.length*2); i+=2){

        // The turkish verb is always the first one and the translation in Greek is the right next one
        let verbTurkish = $(verbsContainer).find('td:eq('+ i + ')').text();
        let verbGreek = $(verbsContainer).find('td:eq('+ (i+1) +')').text();

        // For the dictionary the structure is: [key:turkish = value:translation]
        dictionary[verbTurkish] = verbGreek;
    }    

    await browser.close();
    return dictionary;
};

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname + '/public/index.html'));
})

app.get('/api/verbs/', async function(req, res){
    const data = await scrapVerbs();
    return res.json({data: data})
});

// Initialize Server to listen
app.listen(process.env.PORT || 3001, function(){
    console.log('Server is listening');
});
